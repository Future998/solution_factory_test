# Настройка проекта
1. Загрузка зависимостей Python.
```sh
python3 -m venv sfvenv
source sfvenv/bin/activate
pip install -r requirements.txt
```
2. Создание БД.
```sh
python interaction_with_db.py 
```
# Запуск проекта
После чего доступен запуск проекта при помощи команды:
```sh
python main.py 
```