class ClientExists(Exception):
    '''
    Client exists
    '''

class ClientDontExists(Exception):
    '''
    Client dont exists
    '''

class DistributionDontExists(Exception):
    '''
    Distribution dont exists
    '''

class MessageDontExists(Exception):
    '''
    Message dont exists
    '''

class EmptyValuesAreEntered(Exception):
    '''
    Empty values are entered
    '''