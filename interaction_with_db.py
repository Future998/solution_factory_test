from datetime import datetime, timedelta
from email import message
from importlib.metadata import distribution
from tracemalloc import stop
from sqlalchemy import Column, Integer, String, ForeignKey, Boolean, create_engine, DateTime, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, Session
from werkzeug.security import generate_password_hash
from excrptions import ClientDontExists, ClientExists, DistributionDontExists, EmptyValuesAreEntered, MessageDontExists
from random import randint
from faker import Faker


engine = create_engine('sqlite:///info_data_base.db', echo=False)
Base = declarative_base(bind=engine)

if __name__ == '__main__':
    Base.metadata.drop_all(engine)


class Distribution(Base):
    __tablename__ = 'distribution'
    id = Column(Integer, primary_key=True)
    start_at = Column(DateTime, nullable=False)
    message_text = Column(String(1024), nullable=False)
    filter_code = Column(String(3), nullable=False)
    filter_tag = Column(String(16), nullable=False)
    stop_at = Column(DateTime, nullable=False)

class Client(Base):
    __tablename__ = 'client'
    id = Column(Integer, primary_key=True)
    phone_number = Column(String(11), nullable=False, unique=True)
    code_of_mobile_operator = Column(String(3), nullable=False)
    tag = Column(String(16), nullable=False)
    timezone = Column(Integer, default=0, nullable=False)

class Message(Base):
    __tablename__ = 'message'
    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, default=datetime.utcnow, nullable=False)
    is_send = Column(Boolean, nullable=False, default=False)
    distribution_id = Column(Integer, ForeignKey('distribution.id', ondelete='CASCADE'), nullable=False)
    client_id = Column(Integer, ForeignKey('client.id', ondelete='CASCADE'), nullable=False)
    
if __name__ == '__main__':
    Base.metadata.create_all()


def add_client(phone_number, tag, timezone):
    phone_number = phone_number.strip()
    if phone_number == '':
        raise EmptyValuesAreEntered
    engine = create_engine('sqlite:///info_data_base.db', echo=False)
    session = Session(bind=engine)
    check_client = session.query(Client.id).filter_by(phone_number=phone_number).first()
    if check_client == None:
        client = Client(phone_number=phone_number, code_of_mobile_operator=phone_number[1:4], tag=tag, timezone=timezone)
        session.add(client)
        session.commit()
    else: 
        raise ClientExists
    session.close()

def update_client(id, new_phone_number, new_tag, new_timezone):
    phone_number = new_phone_number.strip()
    if phone_number == '':
        raise EmptyValuesAreEntered
    engine = create_engine('sqlite:///info_data_base.db', echo=False)
    session = Session(bind=engine)
    client = session.query(Client).get(id)
    if client != None:
        client.phone_number = new_phone_number
        client.code_of_mobile_operator = new_phone_number[1:4]
        client.tag = new_tag
        client.timezone = new_timezone
    else: 
        raise ClientDontExists
    session.commit()        
    session.close()

def del_client(id):
    engine = create_engine('sqlite:///info_data_base.db', echo=False)
    session = Session(bind=engine)
    client = session.query(Client).filter_by(id=id).first()
    session.delete(client)
    session.commit()
    session.close()

def get_stats_distribution():
    engine = create_engine('sqlite:///info_data_base.db', echo=False)
    session = Session(bind=engine)
    dists = session.query(Distribution.id).all()
    rez = []
    for d in dists:
        rez.append({
            'id': d.id,
            'sended': session.query(Message.id).filter_by(distribution_id=d.id, is_send=True).count(),
            'dont_sended': session.query(Message.id).filter_by(distribution_id=d.id, is_send=False).count()
        })
    session.close()
    return rez

def get_stats_details(distribution_id):
    engine = create_engine('sqlite:///info_data_base.db', echo=False)
    session = Session(bind=engine)
    messages = session.query(Message).filter_by(distribution_id=distribution_id).all()
    rez = []
    for m in messages:
        message = {'id': m.id,
        'start_at': m.start_at,
        'message_text': m.message_text,
        'filter_code': m.filter_code,
        'filter_tag': m.filter_tag,
        'stop_at': m.stop_at}
        rez.append(message)
    session.close()
    return rez

def add_distribution(start_at, message_text, filter_code, filter_tag, stop_at):
    message_text = message_text.strip()
    if message_text == '':
        raise EmptyValuesAreEntered
    engine = create_engine('sqlite:///info_data_base.db', echo=False)
    session = Session(bind=engine)
    distribution = Distribution(start_at=start_at, message_text=message_text, filter_code=filter_code, filter_tag=filter_tag, stop_at=stop_at)
    session.add(distribution)
    session.commit()
    dist_id = distribution.id
    session.close()
    return dist_id

def update_distribution(id, new_start_at, new_message_text, new_filter_code, new_filter_tag, new_stop_at):
    message_text = new_message_text.strip()
    if message_text == '':
        raise EmptyValuesAreEntered
    engine = create_engine('sqlite:///info_data_base.db', echo=False)
    session = Session(bind=engine)
    distribution = session.query(Distribution).get(id)
    if distribution != None:
        distribution.start_at = new_start_at
        distribution.message_text = new_message_text
        distribution.filter_code = new_filter_code
        distribution.filter_tag = new_filter_tag
        distribution.stop_at = new_stop_at
    else:
        raise DistributionDontExists
    session.commit()
    session.close()

def del_distribution(id):
    engine = create_engine('sqlite:///info_data_base.db', echo=False)
    session = Session(bind=engine)
    distribution = session.query(Distribution).filter_by(id=id).first()
    session.delete(distribution)
    session.commit()
    session.close()

def gen_messages(distribution_id):
    engine = create_engine('sqlite:///info_data_base.db', echo=False)
    session = Session(bind=engine)
    distribution = session.query(Distribution).filter_by(id=distribution_id).first()
    if distribution.filter_tag == '': 
        clients = session.query(Client.id).filter_by(code_of_mobile_operator=distribution.filter_code).all()
    elif distribution.filter_code == '':
        clients = session.query(Client.id).filter_by(tag=distribution.filter_tag).all()
    else:
        clients = session.query(Client.id).filter_by(tag=distribution.filter_tag, code_of_mobile_operator=distribution.filter_code).all()
    for c in clients:
        check_message = session.query(Message).filter_by(distribution_id=distribution_id, client_id=c.id).all()
        if check_message == []:
            message = Message(distribution_id=distribution_id, client_id=c.id)
            session.add(message)
    session.commit()
    session.close()

def send_message(message_id):
    engine = create_engine('sqlite:///info_data_base.db', echo=False)
    session = Session(bind=engine)
    message = session.query(Message).get(message_id)
    if message_id != None:
        message.is_send = True
    else: 
        raise MessageDontExists
    session.commit()        
    session.close()

def get_actual_distributions():
    engine = create_engine('sqlite:///info_data_base.db', echo=False)
    session = Session(bind=engine)
    distributions = session.query(Distribution.id).filter(Distribution.start_at <= datetime.now(), Distribution.stop_at >= datetime.now()).all()
    rez = []
    for d in distributions:
        rez.append(d.id)
    session.close()
    return rez

def get_unsended_messages():
    engine = create_engine('sqlite:///info_data_base.db', echo=False)
    session = Session(bind=engine)
    dists = get_actual_distributions()
    rez = []
    for d in dists:
        messages = session.query(Message).filter_by(distribution_id=d, is_send=False).all()
        for m in messages:
            client = session.query(Client).get(m.client_id)
            dist = session.query(Distribution).get(m.distribution_id)
            rez.append([m.id, client.phone_number, dist.message_text])
    session.close()
    return rez


if __name__ == '__main__':
    fake = Faker('ru_RU')
    for i in range(1000):
        pn = str(70000000000 + randint(0, 9999999999))
        t = fake.texts(max_nb_chars=16)[0]
        add_client(pn, t, randint(-12, 14))
        print(f'{i} клиент создан')

    for i in range(200):
        start = datetime.now()
        stop = datetime.now() + timedelta(days=2)
        if i % 2 == 0:
            ft = fake.texts(max_nb_chars=16)[0]
            fc = ''
        else:
            ft = ''
            fc = str(1000 + randint(0, 999))[1:4]
        mt = t = fake.texts(max_nb_chars=1024)[0]
        dist_id = add_distribution(start, mt, fc, ft, stop)
        gen_messages(dist_id)
        print(f'{i} рассылка и сообщения для нее созданы')
