from flask import Flask, request, jsonify
from flask_caching import Cache
from flask_sockets import Sockets
from flask_cors import CORS
from threading import Thread
import interaction_with_db
from datetime import datetime
import requests


app = Flask(__name__, template_folder="templates")
# Отмена кэширования статических файлов
app.config["CACHE_TYPE"] = "null"
# Инициализация кэша приложения
cache = Cache(app)
cache.init_app(app)
app.config['UPLOAD_FOLDER'] = "static/"
CORS(app)

app.secret_key = "FIYGRFERBKCYBKEUYVCYECERUYBCRU"
sockets = Sockets(app)
secret_key_for_images = "FHIRLUGIGYRERLVBUV132BJHVLYRFEHRCEBVKRRVJHBVB34"
url = "http://127.0.0.1:5000"


def start_distributions():
    while True:
        distributions = interaction_with_db.get_actual_distributions()
        for d_id in distributions:
            interaction_with_db.gen_messages(d_id)

thread1 = Thread(target=start_distributions)
thread1.start()

def send_masseges():
    url_send = 'https://probe.fbrq.cloud/v1/send/'
    headers = {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTUyOTU0MzgsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IkJBTjk5OCJ9.tjNrFuVmIkd-OPFxZC4GoLtAW-XLR_R-Vpq-3sUg0zc'}
    while True:
        messages = interaction_with_db.get_unsended_messages()
        for m in messages:
            data = {
            'id': m[0],
            'phone': m[1],
            'text': m[2]
            }
            r = requests.post(f'{url_send}{m[0]}', data=data, headers=headers)
            print(r.status_code, f'{url_send}{m[0]}')
            if r.status_code == 200:
                if r.json().get('message') == 'OK' and r.json().get('code') == 0:
                    interaction_with_db.send_message(m[0])

thread2 = Thread(target=send_masseges)
thread2.start()



@app.route("/add_client", methods=["POST"])
def add_client():
    phone_number = request.json['phone_number']
    tag = request.json['tag']
    timezone = request.json['timezone']
    try:
        interaction_with_db.add_client(phone_number, tag, timezone)
        return jsonify({'error': False})
    except:
       return jsonify({'error': True})

# Данные передаются в JSON-формате:
# {
#   "phone_number": string(16),
#   "tag": string(10),
#   "timezone": int
# }

@app.route("/update_client", methods=["POST"])
def update_client():
    client_id = request.json['client_id']
    phone_number = request.json['phone_number']
    tag = request.json['tag']
    timezone = request.json['timezone']
    try:
        interaction_with_db.update_client(client_id, phone_number, tag, timezone)
        return jsonify({'error': False})
    except:
       return jsonify({'error': True})

# Данные передаются в JSON-формате:
# {
#     "client_id": int,
#     "phone_number": string(11),
#     "tag": string(16),
#     "timezone": int
# }

@app.route("/del_client", methods=["DELETE"])
def del_client():
    client_id = request.json['client_id']
    try:
        interaction_with_db.del_client(client_id)
        return jsonify({'error': False})
    except:
       return jsonify({'error': True}) 

# Данные передаются в JSON-формате:
# {
#   "client_id": int
# }

@app.route("/add_distribution", methods=["POST"])
def add_distribution():
    start_at = request.json['start_at']
    start_at = datetime.strptime(start_at, '%Y-%m-%d %H:%M')
    message_text = request.json['message_text']
    filter_code = request.json['filter_code']
    filter_tag = request.json['filter_tag']
    stop_at = request.json['stop_at']
    stop_at = datetime.strptime(stop_at, '%Y-%m-%d %H:%M')
    try:
        dist_id = interaction_with_db.add_distribution(start_at, message_text, filter_code, filter_tag, stop_at)
        start = start_at <= datetime.now()
        stop = stop_at >= datetime.now()
        if start and stop:
            interaction_with_db.gen_messages(dist_id)
        return jsonify({'error': False})
    except:
       return jsonify({'error': True})

# Данные передаются в JSON-формате:
# {
#   "start_at": date -> format "YYYY-MM-DD HH:MM",
#   "message_text": string(1024),
#   "filter_code": string(3),
#   "filter_tag": string(16),
#   "stop_at": date -> format "YYYY-MM-DD HH:MM"
# }

@app.route("/update_distribution", methods=["POST"])
def update_distribution():
    distribution_id = request.json['distribution_id']
    start_at = request.json['start_at']
    start_at = datetime.strptime(start_at, '%Y-%m-%d %H:%M')
    message_text = request.json['message_text']
    filter_code = request.json['filter_code']
    filter_tag = request.json['filter_tag']
    stop_at = request.json['stop_at']
    stop_at = datetime.strptime(stop_at, '%Y-%m-%d %H:%M')
    try:
        interaction_with_db.update_distribution(distribution_id, start_at, message_text, filter_code, filter_tag, stop_at)
        start = start_at <= datetime.now()
        stop = stop_at >= datetime.now()
        if start and stop:
            interaction_with_db.gen_messages(distribution_id)
        return jsonify({'error': False})
    except:
       return jsonify({'error': True})

# Данные передаются в JSON-формате:
# {
#   "distribution_id": int,
#   "start_at": date -> format "YYYY-MM-DD HH:MM",
#   "message_text": string(1024),
#   "filter_code": string(3),
#   "filter_tag": string(16),
#   "stop_at": date -> format "YYYY-MM-DD HH:MM"
# }

@app.route("/del_distribution", methods=["DELETE"])
def del_distribution():
    distribution_id = request.json['distribution_id']
    try:
        interaction_with_db.del_distribution(distribution_id)
        return jsonify({'error': False})
    except:
       return jsonify({'error': True})

# Данные передаются в JSON-формате:
# {
#   "distribution_id": int
# }
       
@app.route("/get_stats_distribution", methods=["GET"])
def get_stats_distribution():
    try:
        rez = interaction_with_db.get_stats_distribution()
        return jsonify(rez)
    except:
       return jsonify({'error': True})

# Дополнительные параметры не передаются

@app.route("/get_stats_details", methods=["POST"])
def get_stats_details():
    distribution_id = request.json['distribution_id']
    try:
        rez = interaction_with_db.get_stats_details(distribution_id)
        return jsonify(rez)
    except:
       return jsonify({'error': True})
       
# Данные передаются в JSON-формате:
# {
#   "distribution_id": int
# }


app.run()